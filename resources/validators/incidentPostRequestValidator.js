let validate = (body) => {

    // Handling empty requests
    if (!body) throw "Empty Request";

    // Handlind Missing parameters
    if (!body.userId || !body.coordinates || body.coordinates.length !== 2) throw "Invalid Request";

    // Handling non-numeric coordinates
    if (typeof body.coordinates[0] !== 'number' || typeof body.coordinates[1] !== 'number') throw "Non-numeric Coordinates";

}
module.exports.validate = validate;