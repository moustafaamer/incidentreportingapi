const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();
const { "v4": uuidv4 } = require('uuid');

const requestValidator = require('../validators/incidentPostRequestValidator');

let buildIncidentObject = (body) => {
    return {
        incidentId: uuidv4(),
        userId: body.userId,
        coordinates: body.coordinates,
        timestamp: Date.now(),
        resolved: false
    };
}

let handle = async (body, envVariables) => {
    try {
        // Validate request body
        requestValidator.validate(body);
    } catch (error) {
        return {
            statusCode: 400,
            body: error
        };
    }

    // Create incident object
    const dbObject = buildIncidentObject(body);

    // Add incident object to dynamo DB
    const dbConfig = {
        TableName: envVariables.dynamoDBTableName,
        Item: dbObject
    }

    await db.put(dbConfig).promise();
    return { 
        statusCode: 200, 
        body: JSON.stringify(dbObject)
    };
}

module.exports.handle = handle;
