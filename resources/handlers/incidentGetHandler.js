const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();
const nodeGeocoder = require('node-geocoder');

/**
 * Converts data in incidentList to be intuitive for users
 */
let renderIncidentList = async (incidentList, envVariables) => {
    const geocoderConfig = {
        provider: envVariables.geocoderProvider,
        apiKey: envVariables.geocoderApiKey
    }
    const geocoder = nodeGeocoder(geocoderConfig);
    for (let index in incidentList) {

        // Convert echo timestamp to DD MM YYYY H:M:S format 
        incidentList[index].timestamp = new Date(incidentList[index].timestamp).toUTCString();

        // Convert coordinates to standard address format
        geocoderResult = await geocoder.reverse({
            lat: incidentList[index].coordinates[0],
            lon: incidentList[index].coordinates[1]    
        });
        geocoderData = geocoderResult[0];
        
        incidentList[index].address = geocoderData.streetName === "" ? "Unknown address" : geocoderData.formattedAddress;
    }
}

let handle = async (envVariables) => {
    // DB configurations
    const dbConfig = {
        TableName: envVariables.dynamoDBTableName 
    }

    // Get results from database
    const databaseResult = await db.scan(dbConfig).promise();
    const incidentList = databaseResult.Items;
    
    // Render incidents to be intuitive to the user
    await renderIncidentList(incidentList, envVariables);

    return { 
        statusCode: 200, 
        body: JSON.stringify(incidentList)
    };

}

module.exports.handle = handle;
