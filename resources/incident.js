const postHandler = require('./handlers/incidentPostHandler');
const getHandler = require('./handlers/incidentGetHandler');

const envVariables = {
    dynamoDBTableName: process.env.DATABASE_TABLE_NAME,
    geocoderProvider: process.env.GEOCODER_PROVIDER,
    geocoderApiKey: process.env.GEOCODER_API_KEY
}

exports.main = async (event, context) => {
    const requestMethod = event.httpMethod;
    const requestBody = JSON.parse(event.body);

    if (requestMethod === "POST") {
        // Delegate to postHandler
        return await postHandler.handle(requestBody, envVariables);
    } else if (requestMethod === "GET") {
        // Delegate to getHandler
        return await getHandler.handle(envVariables);
    } else {
        throw "Unsupported Operation";
    }
}