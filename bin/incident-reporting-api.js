#!/usr/bin/env node

const cdk = require('@aws-cdk/core');
const { IncidentReportingApiStack } = require('../lib/incident-reporting-api-stack');

const app = new cdk.App();
new IncidentReportingApiStack(app, 'IncidentReportingApiStack');
