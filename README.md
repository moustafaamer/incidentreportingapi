# Welcome to Incident-reporting-api

Sometimes people are put in situations where they want to scream "HELP ME" without screaming "HELP ME". Whether a person is being followed, or in a very uncomfortable situation, the Incident-reporting-api will help!

## Business usecase:

- A mobile phone app that saves user information on signup and allows the user to click "HELP ME" button to get out of the tense situation.
- Once the "HELP ME"button is clicked, the location is recorded and other users can find that a person needs help through the app.

## How does this work?

- Once "HELP ME" button is clicked, the app will issue a POST request to the api to create an incident object
- A GET method is available to list all incident objects

## Project walkthrough

This project implements the API assuming the mobile app exists. The project is implemented in JavaScript using the cdk tool kit to deploy the Rest API to AWS. The API is deployed using AWS Lambda and AWS APIGateway. The API uses dynamoDB as the data storage to build a consistent stack through AWS.

### Component structure

_/lib/incidentReportingService.js_ defines the AWS infrastructure

_/resources/incident.js_ is the main method that handles the api requests

_/resources/validators/incidentPostRequestValidator.js_ is responsible for validating the request body

_/resources/handlers/incidentPostHandler.js_ handles creation and insertion of incident object in the database

_/resources/handlers/incidentGetHandler.js_ handles rendering the incident objects to users

### API Methods

#### POST /incident

_Description:_ Creates incident object and adds it to dynamo DB table

_Request body:_

- userId: ID reference to a user object, this project assumes a user object exists
- coordinates: location of person reporting incident

_Responses:_

- success -> statusCode: 200

**_data: _**

incidentId: Unique incident identifier

userId: ID Reference to a user object

coordinates: Location of person reporting incident

timestamp: Epoch timestamp of incident

resolved: Boolean indicator to show if incident is resolved

- Empty Request -> statusCode: 400
- Invalid Request -> statusCode: 400
- Non-numeric coordinates -> statusCode: 400
- Internal Server Error -> statusCode: 500

##### Example:

**POST/incident**

{
"userId": "001",
"coordinates": [49.285448726959494, -123.12244463354526]
}
{
"incidentId": "d73e58b7-91a7-4c62-bf5b-c06659f54036",
"userId": "001",
"coordinates": [49.285448726959494,-123.12244463354526],
"timestamp": 1617023936518,
"resolved": false
}

#### ## GET /incident

_Description:_ Returns list of incidents

_Request parameters:_ None

_Responses:_

- success -> statusCode: 200

**_data: _**

incidentId: Unique incident identifier

resolved: Boolean indicator to show if incident is resolved

userId: ID reference to user object

timestamp: date time in DD MM YYYY HH : MM : SS format

address: Location of user reporting incident in standard address format

- Internal Server Error -> statusCode: 500

##### Example:

**GET /incident**

[
{
"incidentId": "0d946568-f78e-486b-a34f-20bb4c743e3d",
"resolved": false,
"coordinates": [
49.285448726959494,
-123.12244463354526
],
"userId": "001",
"timestamp": "Mon, 29 Mar 2021 12:07:21 GMT",
"address": "1079 W Georgia St, Vancouver, BC V6E 4N4, CA"
}
]

## To run this project

To run this app, you need to be in the project root folder and run the following

**npm install -g aws-cdk**

**npm install**

Navigate to /resources directory and run:

**npm install**

_.env file_
DATABASE_TABLE_NAME=Incidents
DATABASE_PRIMARY_KEY=incidentId
GEOCODER_PROVIDER=mapquest
GEOCODER_API_KEY=aOMXGuUyvlmelFtTQOZPlBAT46HHWqBK

Finally to deploy to your AWS account run the following command:
**cdk deploy**

## Useful cdk commands

- **cdk deploy** deploy this stack to your default AWS account/region
- **cdk diff** compare deployed stack with current state
- **cdk synth** emits the synthesized CloudFormation template
