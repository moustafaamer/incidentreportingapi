const core = require('@aws-cdk/core');
const apigateway = require('@aws-cdk/aws-apigateway');
const lambda = require('@aws-cdk/aws-lambda');
const dynamodb = require('@aws-cdk/aws-dynamodb');

// Load environment variables
require('dotenv').config();

const dynamoDBEnvVariables = {
    tableName: process.env.DATABASE_TABLE_NAME || 'Incidents',
    primaryKey: process.env.DATABASE_PRIMARY_KEY || 'incidentId'
}

class IncidentReportingService extends core.Construct {
    constructor(scope, id) {
        super(scope, id);
        // Create AWS infrastructure

        // Create dynamoDB Table
        const dynamoDBTable = new dynamodb.Table(this, dynamoDBEnvVariables.tableName, {
            partitionKey: {
                name: dynamoDBEnvVariables.primaryKey,
                type: dynamodb.AttributeType.STRING
            },
            tableName: dynamoDBEnvVariables.tableName
        })

        // Create Lambda Function
        const incidentLambdaFunction = new lambda.Function(this, 'incidentHandler', {
            runtime: lambda.Runtime.NODEJS_10_X,
            code: lambda.Code.fromAsset('resources'),
            handler: 'incident.main',
            environment: {
                DATABASE_TABLE_NAME: dynamoDBEnvVariables.tableName,
                GEOCODER_PROVIDER: process.env.GEOCODER_PROVIDER,
                GEOCODER_API_KEY: process.env.GEOCODER_API_KEY
            }
        });
        
        // Grant lambda function read-write permissions to dynamoDB table
        dynamoDBTable.grantReadWriteData(incidentLambdaFunction);

        // Create API gateway
        const api = new apigateway.RestApi(this, 'incident-reporting-api', {
            restApiName: 'incident-reporting-api',
            description: 'This service manages incident operations'
        });

        // Integrate lambda function created with the API gateway
        const incidentLambdaApiIntegration = new apigateway.LambdaIntegration(incidentLambdaFunction, {
            requestTemplates: { 
                'application/json': '{ "statusCode": 200 }' 
            }
        });

        // Create 'incident' resource
        const incidentResource = api.root.addResource('incident');
        
        // Add POST and GET methods to 'incident' resource
        incidentResource.addMethod("POST", incidentLambdaApiIntegration);
        incidentResource.addMethod("GET", incidentLambdaApiIntegration);
    }
}
module.exports = { IncidentReportingService };