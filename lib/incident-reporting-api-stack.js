const cdk = require('@aws-cdk/core');

const incidentApi = require('../lib/incidentReportingService');
class IncidentReportingApiStack extends cdk.Stack {
  /**
   *
   * @param {cdk.Construct} scope
   * @param {string} id
   * @param {cdk.StackProps=} props
   */
  constructor(scope, id, props) {
    super(scope, id, props);

    new incidentApi.IncidentReportingService(this, 'incidentApi');
  }
}

module.exports = { IncidentReportingApiStack }
